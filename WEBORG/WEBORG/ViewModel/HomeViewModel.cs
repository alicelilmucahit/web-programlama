﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WEBORG.Models;

namespace WEBORG.ViewModel
{
    public class HomeViewModel
    {
        public List<Urunler> Urunler { get; set; }
        public List<Kategoriler> Kategoriler { get; set; }
        public List<Kisiler> Kisiler { get; set; }
        public List<Order> Order { get; set; }
    }
}