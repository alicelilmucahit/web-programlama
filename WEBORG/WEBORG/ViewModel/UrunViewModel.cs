﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WEBORG.Models;

namespace WEBORG.ViewModel
{
    public class UrunViewModel
    {
        public Urunler Urun { get; set; }

        public List<Yorumlar> Yorumlar { get; set; }
    }
}