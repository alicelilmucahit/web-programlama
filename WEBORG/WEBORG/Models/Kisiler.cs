﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WEBORG.Models
{
    [Table("Kisiler")]
    public class Kisiler
    {
        [Required,
             DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Required(ErrorMessageResourceType = (typeof(WEBORG.Resources.Lang)), ErrorMessageResourceName = "error_empty"),
            DisplayName("Kullanıcı Adı"),
            MinLength(5, ErrorMessageResourceType = (typeof(WEBORG.Resources.Lang)),ErrorMessageResourceName ="error_userid_min"),
            MaxLength(20, ErrorMessageResourceType = (typeof(WEBORG.Resources.Lang)), ErrorMessageResourceName = "error_userid_max")]
        public string KullanıcıAdı { get; set; }
        [DisplayName("Adı"),
            MinLength(3, ErrorMessageResourceType = (typeof(WEBORG.Resources.Lang)), ErrorMessageResourceName = "error_id_min"),
            MaxLength(20, ErrorMessageResourceType = (typeof(WEBORG.Resources.Lang)), ErrorMessageResourceName = "error_id_max")]
        public string Ad { get; set; }
        [DisplayName("Soyadı"),
            Required(ErrorMessageResourceType = (typeof(WEBORG.Resources.Lang)), ErrorMessageResourceName = "error_empty"),
            MinLength(3, ErrorMessageResourceType = (typeof(WEBORG.Resources.Lang)), ErrorMessageResourceName = "error_surname_min"),
            MaxLength(20, ErrorMessageResourceType = (typeof(WEBORG.Resources.Lang)), ErrorMessageResourceName = "error_surname_max")]
        public string Soyad { get; set; }
        [DisplayName("Eposta"),
            Required(ErrorMessageResourceType = (typeof(WEBORG.Resources.Lang)), ErrorMessageResourceName = "error_empty"),
            MaxLength(60, ErrorMessageResourceType = (typeof(WEBORG.Resources.Lang)), ErrorMessageResourceName = "error_mail"),
            EmailAddress(ErrorMessageResourceType = (typeof(WEBORG.Resources.Lang)), ErrorMessageResourceName = "error_mail_correct")]
        public string Email { get; set; }
        [DisplayName("Şifre"),
            Required(ErrorMessageResourceType = (typeof(WEBORG.Resources.Lang)), ErrorMessageResourceName = "error_empty"),
            MinLength(8, ErrorMessageResourceType = (typeof(WEBORG.Resources.Lang)), ErrorMessageResourceName = "error_pass_min"),
            MaxLength(16, ErrorMessageResourceType = (typeof(WEBORG.Resources.Lang)), ErrorMessageResourceName = "error_pass_max"),
            DataType(DataType.Password, ErrorMessage = "Lütfen {0} degerini doğru giriniz!")]
        public string Sifre { get; set; }
        [DisplayName("Şifre(Tekrar)"),
            Required(ErrorMessageResourceType = (typeof(WEBORG.Resources.Lang)), ErrorMessageResourceName = "error_empty"),
            MinLength(8, ErrorMessageResourceType = (typeof(WEBORG.Resources.Lang)), ErrorMessageResourceName = "error_pass_min"),
            MaxLength(16, ErrorMessageResourceType = (typeof(WEBORG.Resources.Lang)), ErrorMessageResourceName = "error_pass_max"),
            DataType(DataType.Password, ErrorMessage = "Lütfen {0} degerini doğru giriniz!"),
            Compare(nameof(Sifre), ErrorMessageResourceType = (typeof(WEBORG.Resources.Lang)), ErrorMessageResourceName = "error_pass_compare")]
        public string SifreTekrar { get; set; }
    }
}