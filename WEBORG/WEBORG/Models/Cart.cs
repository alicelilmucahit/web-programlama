﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEBORG.Models
{
    public class Cart
    {
        private List<CartLine> _cartLines = new List<CartLine>();

        public List<CartLine> CartLines
        {
            get { return _cartLines; }
        }

        public void UrunEkle(Urunler urun, int adet)
        {
            var line = _cartLines.Where(x => x.Urunler.UrunId == urun.UrunId).FirstOrDefault();
            if (line == null)
            {
                _cartLines.Add(new CartLine() { Urunler = urun, Adet = adet });
            }
            else
            {
                line.Adet += adet;
            }
        }

        public void UrunCıkar(Urunler urun)
        {
            _cartLines.RemoveAll(x => x.Urunler.UrunId == urun.UrunId);
        }

        public double ToplamUcret()
        {
            return _cartLines.Sum(x => x.Urunler.UrunUcreti * x.Adet);
        }

        public void SepetiBosalt()
        {
            _cartLines.Clear();
        }
    }

    public class CartLine
    {
        public Urunler Urunler { get; set; }
        public int Adet { get; set; }
    }
}