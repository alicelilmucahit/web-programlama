﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WEBORG.Models
{
    [Table("Kategoriler")]
    public class Kategoriler
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int KategoriId { get; set; }
        [Required(ErrorMessageResourceType = (typeof(WEBORG.Resources.Lang)), ErrorMessageResourceName = "error_empty")]
        public string KategoriAdi { get; set; }

        public virtual List<Urunler> Urunler { get; set; }
    }
}