﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WEBORG.Models
{
    public class Order
    {
        public int Id { get; set; }
        public String SiparisNo { get; set; }
        public double Toplam { get; set; }
        public DateTime OrderDate { get; set; }
        [Required(ErrorMessageResourceType = (typeof(WEBORG.Resources.Lang)), ErrorMessageResourceName = "error_empty")]
        public string UserName { get; set; }
        [Required(ErrorMessageResourceType = (typeof(WEBORG.Resources.Lang)), ErrorMessageResourceName = "error_empty")]
        public string Adres { get; set; }
        [Required(ErrorMessageResourceType = (typeof(WEBORG.Resources.Lang)), ErrorMessageResourceName = "error_empty")]
        public string Sehir { get; set; }
        [Required(ErrorMessageResourceType = (typeof(WEBORG.Resources.Lang)), ErrorMessageResourceName = "error_empty")]
        public string PostaKodu { get; set; }

        public virtual List<OrderLine> Orderlines { get; set; }
    }

    public class OrderLine
    {
        public int Id { get; set; }
        public virtual Order Order { get; set; }
        public int Adet { get; set; }
        public double ucret { get; set; }
        public int UrunId { get; set; }
        public virtual Urunler Urunler { get; set; }

    }
}