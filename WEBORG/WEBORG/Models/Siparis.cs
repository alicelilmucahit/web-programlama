﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WEBORG.Models
{
    public class Siparis
    {
        public string UserName { get; set; }
        [Required(ErrorMessageResourceType = (typeof(WEBORG.Resources.Lang)), ErrorMessageResourceName = "error_adress")]
        public string Adres { get; set; }
        [Required(ErrorMessageResourceType = (typeof(WEBORG.Resources.Lang)), ErrorMessageResourceName = "error_city")]
        public string Sehir { get; set; }
        [Required(ErrorMessageResourceType = (typeof(WEBORG.Resources.Lang)), ErrorMessageResourceName = "error_postal")]
        public string PostaKodu { get; set; }
    }
}