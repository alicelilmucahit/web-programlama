﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WEBORG.Models
{
    public class Yorumlar
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int UrunId { get; set; }
        [Required]
        public string KullanıcıAdı { get; set; }
        [Required]
        public DateTime YorumTarihi { get; set; }
        [Required]
        public string Yorum { get; set; }
    }
}