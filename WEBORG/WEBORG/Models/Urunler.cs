﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WEBORG.Models
{
    [Table("Urunler")]
    public class Urunler
    {
        [Key(), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UrunId { get; set; }
        [Required(ErrorMessageResourceType = (typeof(WEBORG.Resources.Lang)), ErrorMessageResourceName = "error_empty")]
        public String UrunAdı { get; set; }
        [Required(ErrorMessageResourceType = (typeof(WEBORG.Resources.Lang)), ErrorMessageResourceName = "error_empty")]
        public int UrunUcreti { get; set; }

        public string UrunResmi { get; set; }
        [Required(ErrorMessageResourceType = (typeof(WEBORG.Resources.Lang)), ErrorMessageResourceName = "error_empty")]
        public string UrunAciklama { get; set; }

        public virtual Kategoriler Kategori { get; set; }
    }
}