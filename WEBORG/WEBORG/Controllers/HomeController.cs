﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using WEBORG.Filters;
using WEBORG.Models;
using WEBORG.Models.Managers;
using WEBORG.ViewModel;

namespace WEBORG.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        DatabaseContext db = new DatabaseContext();
        HomeViewModel mdl = new HomeViewModel();
        public ActionResult Index()
        {
            mdl.Urunler = db.Urunler.ToList();
            mdl.Kategoriler = db.Kategoriler.ToList();
            return View(mdl);
        }

        public ActionResult Change(String languageSelect)
        {
            if (languageSelect != null)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageSelect);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageSelect);
            }

            HttpCookie cookie = new HttpCookie("language");
            cookie.Value = languageSelect;
            Response.Cookies.Add(cookie);

            return RedirectToAction("Index");
        }

        public ActionResult Urun(int? urunId)
        {
            if (urunId != null)
            {
                UrunViewModel umdl = new UrunViewModel();
                umdl.Yorumlar = db.Yorumlar.Where(x => x.UrunId == urunId).ToList() ;
                umdl.Urun = db.Urunler.Where(x => x.UrunId == urunId).FirstOrDefault();
                return View(umdl);
            }
            return View();
        }
        [HttpPost]
        public ActionResult Urun(int urunId, string yorum, string KullanıcıAdı)
        {
            Yorumlar y = new Yorumlar();
            var urun = db.Urunler.Where(x => x.UrunId == urunId).FirstOrDefault();
            y.UrunId = urun.UrunId;
            y.YorumTarihi = DateTime.Now;
            y.Yorum = yorum;
            y.KullanıcıAdı = KullanıcıAdı;
            db.Yorumlar.Add(y);
            db.SaveChanges();
            return Urun(urunId);
        }

        public ActionResult Filtrele(int? kategoriId, string kategori)
        {
            if (kategoriId != null)
            {
                ViewBag.Kategori = kategori;
                List<Urunler> urunler = new List<Urunler>();
                urunler = db.Urunler.Where(x => x.Kategori.KategoriId == kategoriId).ToList();
                if (urunler.Count == 0)
                    ViewBag.Sonuc =WEBORG.Resources.Lang.notproduct;
                else
                    ViewBag.Sonuc = "";
                return View(urunler);
            }
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string KullanıcıAdı, string Sifre)
        {
            Kisiler uye = null;
            if (KullanıcıAdı != null && Sifre != null)
            {
                if (KullanıcıAdı == "admin" && Sifre == "12345678")
                {
                    Session["KullanıcıAdı"] = "admin";
                    return RedirectToAction("Index", "Yonetim");
                }
                if (KullanıcıAdı == "moderator" && Sifre == "12345678")
                {
                    Session["KullanıcıAdı"] = "moderator";
                    return RedirectToAction("Index", "Yonetim");
                }
                uye = db.Kisiler.Where(x => x.KullanıcıAdı == KullanıcıAdı && x.Sifre == Sifre).FirstOrDefault();
                ViewBag.Mesaj = null;
                if (uye != null)
                {

                    Session["KullanıcıAdı"] = uye.KullanıcıAdı;
                    Session["Adı"] = uye.Ad;
                    Session["Soyadı"] = uye.Soyad;
                    Session["Email"] = uye.Email;
                    Session["kisiId"] = uye.ID;
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.Mesaj = WEBORG.Resources.Lang.notuser;
                    ViewBag.Alert = "danger";
                    return View();
                }
            }
            return View();
        }

        [AuthFilter]
        public new ActionResult User()
        {
            string isim = Session["KullanıcıAdı"].ToString();
            List<Order> siparis = new List<Order>();
            siparis = db.Order.Where(x => x.UserName == isim).ToList();
            return View(siparis);
        }

        public ActionResult SiparisDetay(int? siparisId)
        {
            var siparis = db.Order.Where(x => x.Id == siparisId).FirstOrDefault();
            return View(siparis);
        }

        public ActionResult Sign()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Sign(Kisiler uye)
        {
            if (ModelState.IsValid)
            {
                db.Kisiler.Add(uye);
                if (db.SaveChanges() > 0)
                {
                    return RedirectToAction("Login");
                }
            }
            return View(uye);
        }

        public ActionResult Sepet()
        {
            return View(GetCart());
        }

        public ActionResult SepeteEkle(int? urunId)
        {
            var urun = db.Urunler.Where(x => x.UrunId == urunId).FirstOrDefault();
            if (urun != null)
            {
                GetCart().UrunEkle(urun, 1);
            }
            return RedirectToAction("Index");
        }

        public ActionResult SepettenCıkar(int? urunId)
        {
            var urun = db.Urunler.Where(x => x.UrunId == urunId).FirstOrDefault();
            if (urun != null)
            {
                GetCart().UrunCıkar(urun);
            }
            return RedirectToAction("Sepet");
        }

        public Cart GetCart()
        {
            var cart = (Cart)Session["cart"];
            if (cart == null)
            {
                cart = new Cart();
                Session["cart"] = cart;
            }
            return cart;
        }

        public ActionResult OdemeYap()
        {
            return View(new Siparis());
        }
        [HttpPost]
        public ActionResult OdemeYap(Siparis s)
        {
            var cart = GetCart();
            if (Session["KullanıcıAdı"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (cart.CartLines.Count() == 0)
            {
                ModelState.AddModelError("UrunYokError", "Sepetinizde Ürün Bulunmamaktadır");
            }
            if (ModelState.IsValid)
            {
                var order = new Order();
                order.SiparisNo = "A" + (new Random()).Next(111111, 999999).ToString();
                order.Toplam = cart.ToplamUcret();
                order.OrderDate = DateTime.Now;

                order.UserName = Session["KullanıcıAdı"].ToString();
                order.Adres = s.Adres;
                order.Sehir = s.Sehir;
                order.PostaKodu = s.PostaKodu;
                order.Orderlines = new List<OrderLine>();
                foreach (var item in cart.CartLines)
                {
                    var orderline = new OrderLine();
                    orderline.Adet = item.Adet;
                    orderline.ucret = item.Adet * item.Urunler.UrunUcreti;
                    orderline.UrunId = item.Urunler.UrunId;

                    order.Orderlines.Add(orderline);
                }
                db.Order.Add(order);
                db.SaveChanges();
                cart.SepetiBosalt();
                return View("Completed");
            }
            else
            {
                return View(s);
            }

        }

        public ActionResult Completed()
        {
            return View();
        }

        public ActionResult CikisYap()
        {
            Session.Clear();
            return RedirectToAction("Index", "Home");
        }
    }
}