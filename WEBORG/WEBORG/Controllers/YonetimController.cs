﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WEBORG.Filters;
using WEBORG.Models;
using WEBORG.Models.Managers;
using WEBORG.ViewModel;

namespace WEBORG.Controllers
{
    public class YonetimController : Controller
    {
        // GET: Yonetim
        [AuthFilter]
        public ActionResult Index()
        {
                DatabaseContext db = new DatabaseContext();
                HomeViewModel mdl = new HomeViewModel();
                mdl.Urunler = db.Urunler.ToList();
                mdl.Kisiler = db.Kisiler.ToList();
                mdl.Kategoriler = db.Kategoriler.ToList();
                mdl.Order = db.Order.ToList();
                return View(mdl);
        }

        [AuthFilter]
        public ActionResult Urunler()
        {
            HomeViewModel mdl = new HomeViewModel();
            DatabaseContext db = new DatabaseContext();
            mdl.Urunler = db.Urunler.ToList();
            return View(mdl);
        }

        List<SelectListItem> kategoriList = new List<SelectListItem>();
        [AuthFilter]
        public ActionResult UrunEkle()
        {
            DatabaseContext db = new DatabaseContext();
            List<Kategoriler> kategoriler = db.Kategoriler.ToList();
            foreach (Kategoriler kategori in kategoriler)
            {
                SelectListItem item = new SelectListItem();
                item.Text = kategori.KategoriAdi;
                item.Value = kategori.KategoriId.ToString();
                kategoriList.Add(item);
            }
            ViewBag.Kategoriler = kategoriList;
            return View();
        }
        [HttpPost]
        public ActionResult UrunEkle(Urunler urun)
        {
            DatabaseContext db = new DatabaseContext();
            if (urun.UrunAdı!=null&& urun.UrunAciklama != null && urun.UrunUcreti != null && urun.Kategori.KategoriId != null)
            {
                var kategori = db.Kategoriler.Where(x => x.KategoriId == urun.Kategori.KategoriId).FirstOrDefault();
                urun.Kategori = kategori;
                urun.UrunResmi = "http://placehold.it/700x400";
                db.Urunler.Add(urun);
                db.SaveChanges();
                return RedirectToAction("Urunler", "Yonetim");
            }
            List<Kategoriler> kategoriler = db.Kategoriler.ToList();
            foreach (Kategoriler kategori in kategoriler)
            {
                SelectListItem item = new SelectListItem();
                item.Text = kategori.KategoriAdi;
                item.Value = kategori.KategoriId.ToString();
                kategoriList.Add(item);
            }
            ViewBag.Kategoriler = kategoriList;
            return View(urun);
        }

        [AuthFilter]
        public ActionResult UrunSil(int? urunId)
        {
            if (urunId != null)
            {
                DatabaseContext db = new DatabaseContext();
                var urun = db.Urunler.Where(x => x.UrunId == urunId).FirstOrDefault();
                db.Urunler.Remove(urun);
                db.SaveChanges();
            }
            return RedirectToAction("Urunler", "Yonetim");
        }

        [AuthFilter]
        public ActionResult UrunGuncelle(int? urunId)
        {
            if (urunId != null)
            {
                DatabaseContext db = new DatabaseContext();
                var urun = db.Urunler.Where(x => x.UrunId == urunId).FirstOrDefault();
                return View(urun);
            }
            return RedirectToAction("Urunler", "Yonetim");
        }
        [HttpPost]
        public ActionResult UrunGuncelle(Urunler u, int? urunId)
        {
            DatabaseContext db = new DatabaseContext();
            Urunler urun = db.Urunler.Where(x => x.UrunId == urunId).FirstOrDefault();
            if (urun != null)
            {
                urun.UrunAdı = u.UrunAdı;
                urun.UrunAciklama = u.UrunAciklama;
                urun.UrunUcreti = u.UrunUcreti;
                urun.UrunResmi = u.UrunResmi;
                if (db.SaveChanges() > 0)
                {
                    ViewBag.Result = "Kişi Başarıyla Güncellenmiştir.";
                    ViewBag.Status = "success";
                }
                else
                {
                    ViewBag.Result = "Kişi Güncellenememiştir.";
                    ViewBag.Status = "danger";
                }
            }
            return View();
        }

        [AuthFilter]
        public ActionResult KategoriEkle()
        {
            return View();
        }
        [HttpPost]
        public ActionResult KategoriEkle(Kategoriler kategori)
        {
            if (ModelState.IsValid)
            {
                DatabaseContext db = new DatabaseContext();
                db.Kategoriler.Add(kategori);
                db.SaveChanges();
            }
            return RedirectToAction("Urunler", "Yonetim");
        }

        [AuthFilter]
        public ActionResult CıkısYap()
        {
            Session.Clear();
            return RedirectToAction("Index", "Home");
        }
    }
}